#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

webservd_root := $(my-dir)

# Definitions applying to all targets. $(eval) this last.
define webservd_common
  LOCAL_CPP_EXTENSION := .cc
  LOCAL_CFLAGS += -Wall -Werror
  LOCAL_AIDL_INCLUDES += $(webservd_root)/aidl

  # libbrillo's secure_blob.h calls "using Blob::vector" to expose its base
  # class's constructors. This causes a "conflicts with version inherited from
  # 'std::__1::vector<unsigned char>'" error when building with GCC.
  LOCAL_CLANG := true

  LOCAL_C_INCLUDES += \
    $(webservd_root) \
    external/gtest/include \

endef  # webserv_common

define webservd_common_libraries
  LOCAL_SHARED_LIBRARIES += \
      libbrillo \
      libbrillo-http \
      libbrillo-stream \
      libchrome \
      libmicrohttpd \
      libbrillo-binder \
      libcutils \
      libutils \
      libbinderwrapper \
      libbinder
  LOCAL_STATIC_LIBRARIES += libwebserv_common

endef  # webserv_common_libraries

include $(call all-subdir-makefiles)
