// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package android.webservd;

interface IHttpRequest {
  // The URL being requested
  @utf8InCpp String GetUrl();

  // The HTTP Request Method
  @utf8InCpp String GetMethod();

  // The HTTP Headers
  @utf8InCpp String[] GetHeaders();

  // The HTTP POST parameters
  @utf8InCpp String[] GetPostParams();

  // The URL parameters
  @utf8InCpp String[] GetQueryParams();

  // Socket that will pass the request body
  FileDescriptor GetBody();

  // Send a response to this request
  FileDescriptor Respond(
    in int status_code,
    in @utf8InCpp String[] response_headers,
    in int data_size);
}
