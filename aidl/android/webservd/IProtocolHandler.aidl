// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package android.webservd;

import android.webservd.IRequestHandler;

interface IProtocolHandler {
  // Add a new request handler. See aidl/android/webservd/IRequestHandler.aidl
  @utf8InCpp String AddRequestHandler(in @utf8InCpp String url,
                                      in @utf8InCpp String method,
                                      in IRequestHandler handler);

  // Remove a request handler.
  void RemoveRequestHandler(in @utf8InCpp String guid);

  // Get the name of this protocol handler.
  @utf8InCpp String GetName();

  // Get the port this protocol handler operates on.
  int GetPort();

  // Get the name of the protocol this handler handles.
  @utf8InCpp String GetProtocol();

  // Get the certificate fingerprint of this handler, empty if this handler
  // is not handling an encrypted protocol.
  byte[] GetCertificateFingerprint();
}
