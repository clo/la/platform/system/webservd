// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "libwebserv/binder_request_handler.h"

#include "libwebserv/binder_request_impl.h"
#include "libwebserv/binder_response.h"

using android::binder::Status;
using android::sp;
using android::webservd::IHttpRequest;
using std::string;
using std::unique_ptr;
using std::vector;

namespace libwebserv {

Status BinderRequestHandler::ProcessRequest(const sp<IHttpRequest>& request) {
  Status status;
  string url;
  string method;
  vector<string> headers;
  vector<string> query_params;
  vector<string> post_params;
  android::base::unique_fd body;

  // TODO(b/27407721): Fewer binder calls here
  status = request->GetUrl(&url);
  if (!status.isOk()) {
    return status;
  }

  status = request->GetMethod(&method);
  if (!status.isOk()) {
    return status;
  }

  status = request->GetHeaders(&headers);
  if (!status.isOk()) {
    return status;
  }

  if (headers.size() % 2) {
    return Status::fromExceptionCode(Status::EX_ILLEGAL_ARGUMENT);
  }

  status = request->GetQueryParams(&query_params);
  if (!status.isOk()) {
    return status;
  }

  if (query_params.size() % 2) {
    return Status::fromExceptionCode(Status::EX_ILLEGAL_ARGUMENT);
  }

  status = request->GetPostParams(&post_params);
  if (!status.isOk()) {
    return status;
  }

  if (post_params.size() % 2) {
    return Status::fromExceptionCode(Status::EX_ILLEGAL_ARGUMENT);
  }

  status = request->GetBody(&body);
  if (!status.isOk()) {
    return status;
  }

  unique_ptr<Request> internal_request(
      new BinderRequestImpl(url, method, headers, query_params,
                            post_params, std::move(body)));
  unique_ptr<Response> internal_response(new BinderResponse(request));

  handler_->HandleRequest(std::move(internal_request),
                          std::move(internal_response));

  return Status::ok();
}

}  // namespace libwebserv
