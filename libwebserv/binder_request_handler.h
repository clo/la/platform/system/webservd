// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WEBSERVER_LIBWEBSERV_BINDER_REQUEST_HANDLER_H_
#define WEBSERVER_LIBWEBSERV_BINDER_REQUEST_HANDLER_H_

#include "android/webservd/BnRequestHandler.h"
#include "libwebserv/request_handler_interface.h"

namespace libwebserv {

class BinderRequestHandler : public android::webservd::BnRequestHandler {
 public:
  explicit BinderRequestHandler(
      libwebserv::RequestHandlerInterface* handler)
      : handler_(handler) {}

  android::binder::Status ProcessRequest(
      const android::sp<android::webservd::IHttpRequest>& request) override;

 private:
  libwebserv::RequestHandlerInterface* handler_;

  DISALLOW_COPY_AND_ASSIGN(BinderRequestHandler);
};

}  // namespace libwebserv

#endif  // WEBSERVER_LIBWEBSERV_BINDER_REQUEST_HANDLER_H_

