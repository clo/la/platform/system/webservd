#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(my-dir)

# TODO: Refactor to build and run unit tests.

# webservd executable
# ========================================================

include $(CLEAR_VARS)
LOCAL_MODULE := webservd
LOCAL_SHARED_LIBRARIES := \
    libcrypto \
    libwebserv

ifdef BRILLO

LOCAL_SHARED_LIBRARIES += \
    libkeymaster_messages \
    libkeystore_binder \

endif

LOCAL_SRC_FILES := \
    config.cc \
    error_codes.cc \
    log_manager.cc \
    main.cc \
    protocol_handler.cc \
    request.cc \
    temp_file_manager.cc \
    utils.cc \

LOCAL_SRC_FILES += \
    ../aidl/android/webservd/IServer.aidl \
    ../aidl/android/webservd/IProtocolHandler.aidl \
    ../aidl/android/webservd/IRequestHandler.aidl \
    ../aidl/android/webservd/IHttpRequest.aidl \
    binder_request.cc \
    binder_server.cc
LOCAL_SHARED_LIBRARIES += \
    libfirewalld-binder-client

ifdef BRILLO
LOCAL_SRC_FILES += keystore_encryptor.cc
else
LOCAL_SRC_FILES += fake_encryptor.cc
endif

LOCAL_INIT_RC := webservd.rc

$(eval $(webservd_common))
$(eval $(webservd_common_libraries))
include $(BUILD_EXECUTABLE)
