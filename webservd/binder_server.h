// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef WEBSERVER_WEBSERVD_BINDER_SERVER_H_
#define WEBSERVER_WEBSERVD_BINDER_SERVER_H_

#include <string>
#include <vector>

#include "webservd/config.h"
#include "webservd/encryptor.h"
#include "webservd/server_interface.h"
#include "webservd/temp_file_manager.h"

#include "android/webservd/BnServer.h"
#include "android/webservd/IProtocolHandler.h"

#include <binderwrapper/binder_wrapper.h>

namespace webservd {

class BinderServer : public android::webservd::BnServer,
                     public webservd::ServerInterface {
 public:
  explicit BinderServer(const Config& config,
                        android::BinderWrapper* binder_wrapper);

  // Overrides from BnServer
  android::binder::Status Ping(std::string* result) override;
  android::binder::Status GetDefaultRequestTimeout(
      int32_t* timeout_seconds) override;
  android::binder::Status GetProtocolHandlers(
      const std::string& name,
      std::vector<android::sp<android::IBinder>>* result)
      override;

  // Overrides from ServerInterface
  void ProtocolHandlerStarted(ProtocolHandler* /*handler*/) override {}
  void ProtocolHandlerStopped(ProtocolHandler* /*handler*/) override {}
  const Config& GetConfig() const override { return config_; }
  TempFileManager* GetTempFileManager() override { return &temp_file_manager_; }

  // Allows injection of a non-default |encryptor| (used for testing). The
  // caller retains ownership of the pointer.
  void SetEncryptor(Encryptor* encryptor) {
    encryptor_ = encryptor;
  }

 private:
  base::FilePath GetUploadDirectory() const;
  void InitTlsData();

  Config config_;
  brillo::Blob TLS_certificate_;
  brillo::Blob TLS_certificate_fingerprint_;
  brillo::SecureBlob TLS_private_key_;
  std::unique_ptr<Encryptor> default_encryptor_;
  Encryptor* encryptor_;
  FileDeleter file_deleter_;
  TempFileManager temp_file_manager_{GetUploadDirectory(), &file_deleter_};

  std::vector<android::sp<android::webservd::IProtocolHandler>>
      protocol_handlers_;

  DISALLOW_COPY_AND_ASSIGN(BinderServer);
};

}  // namespace webservd

#endif  // WEBSERVER_WEBSERVD_BINDER_SERVER_H_
